/*
 * graph.c
 *
 *  Created on: Mar 1, 2016
 *      Author: zgeorg03
 */

#include"graph.h"

long int size_of_graph(Graph *graph) {
	return sizeof(graph) + sizeof(graph->clusters);
}


// We need this! Allocating and deallocating takes time so we need to do it once
long long int* dtable;

// The number of qThreads we can create
int qthreadBudget;

/*
 * Initialize cluster table so that we know which
 *  cluster nodes are uninitialized
 */
void initGraph(Graph* graph) {

	int i;
	for (i = 0; i < (1 << CLTABLEBITS); i++) {
		graph->clusters[i] = 0;
	}

	// Allocate space for the depth table
	int threads = NUMBER_OF_THREADS;
	dtable = (long long int*)malloc(sizeof(long long int)*threads);
	for(i=0; i<threads; i++){
		dtable[i] = -1;
	}

	qthreadBudget = threads;
}

int num_of_clusters(Graph *graph) {
	int i = 0;
	int sum = 0;
	for (i = 0; i < 1 << CLTABLEBITS; i++) {
		if (graph->clusters[i] != 0) {
			sum++;
		}
	}
	return sum;
}

void printGraph(Graph* graph) {

	int i = 0;
	printf("Graph: \n");
	for (i = 0; i < 1 << CLTABLEBITS; i++) {
		// Non empty cluster
		if (!graph->clusters[i])
			continue;

		CLUSTER_NODE* cluster = graph->clusters[i];
		if (!cluster)
			continue;

		for (int j = 0; j < (1 << CLNODEBITS); j++) {

			if (!cluster->neighbors[j])
				continue;

			//printf("Test %d\n",j);

			NEIGHBOR_NODE* neighborList = cluster->neighbors[j];

			unsigned int vertexId = j + (i * (1 << CLNODEBITS));
			printf("(%d)..Vertex(%u): ", i, vertexId);
			printNeighborList(neighborList);

		}

	}
}

int shortest_path(Graph *graph, VisitedTable *vtable,Queue *q, unsigned int u,
		unsigned int v) {

	//printf("Serials! \n");
	//fflush(stdout);

	NEIGHBOR_NODE *neigh = NULL;
	unsigned int x = 0;

	unsigned int depth = 0;

	//Insert the first node in the queue

	enqueue(q, getNeighbors(graph, u), u, 0);
	setVisited(vtable, u);

	//printf("Queuesize: %d\n",q->size);
	//printf("QueueIsEmpty: %d\n",isEmpty(q));
	while (!isEmpty(q)) {

		dequeue(q, &neigh, &x, &depth);
		// INLINE :P
		/*
		 {
		 Node* temp = q->front;
		 if (q->front == q->rear) {
		 q->rear = NULL;
		 q->front = NULL;
		 } else
		 q->front = q->front->next;
		 (q->size)--;
		 neigh = temp->neighbors;
		 depth = temp->depth;
		 x = temp->vertex;
		 free(temp);
		 temp = NULL;
		 }
		 */
		if (x == v) {


			//printf("Traversed THIS far but my journey ends: %d\n",depth);

			emptyQueue(q);
			//clearQueue(&q);
			clearVisitedTable(vtable);
			return depth;
		}
		while (neigh != NULL) {
			for (int i = 0; i < neigh->size; i++) {
				unsigned int y = neigh->neighbors[i];
				if (!isVisited(vtable, y)) {
					setVisited(vtable, y);
					enqueue(q, getNeighbors(graph, y), y, depth + 1);
				}
			}
			neigh = neigh->next;
		}
	}

	emptyQueue(q);
	//clearQueue(&q);
	clearVisitedTable(vtable);


	//printf("Traversed THIS far: %d\n",depth);
	return -1;
}

int shortest_path_parallel(){

	NEIGHBOR_NODE *neigh = NULL;
	unsigned int x = 0;
	unsigned int depth = 0;
	int tid;

	// First get information about the first node's neighbors
	neigh = getNeighbors(graph,u);
	int ncount = getNeighborCount(graph,u);

	// If the ncount is more than a single thread can handle
	if(ncount >= MORE_THAN_A_SINGLE_THREAD_CAN_HANDLE){

		// Check if the 'current thread budget' allows us to cut it in half

	}

}

int shortest_path_parallel(Graph *graph,VisitedTable **vtable,Queue **q, unsigned int u,
		unsigned int v) {

	//printf("Parallels! \n");
	//fflush(stdout);

	NEIGHBOR_NODE *neigh = NULL;
	unsigned int x = 0;

	unsigned int depth = 0;

	// Thread id
	int tid;

	// First get information about the first node's neighbors
	neigh = getNeighbors(graph,u);

	// Assume that all threads visited the first node
	for(int t=0; t<threads; t++)
		setVisited(vtable[t%threads], u);


	//Insert the first node in the queue (in a cyclic fashion)
	int j=0;
	unsigned int i=0;
	while(neigh!=NULL){

		x = neigh->neighbors[j++];

		// In case we find the target here end
		if(x == v){

			for(int t=0; t<threads; t++){
				emptyQueue(q[t]);
				clearVisitedTable(vtable[t]);
			}

			return 1;
		}

		// Enqueue to each queue of a thread
		enqueue(q[i%threads],getNeighbors(graph,x),x,1);
		setVisited(vtable[i%threads], x);

		i++;

		if(j>neigh->size){
			j = 0;
			neigh = neigh->next;
		}

	}

	// A table to store each result
	for(int t=0; t<threads; t++){
		dtable[t] = -1;
	}


	// Now use openmp to parallelize the work
	#pragma omp parallel private(tid,depth)
	{
		tid = omp_get_thread_num();
		// Get depth
		depth = shortest_path_parallel_worker(graph,vtable[tid],q[tid],1,v,threads);
		// Store depth to table
		dtable[tid] = depth;
	}

	// Empty queues and tables for future use
	for(int t=0; t<threads; t++){
		emptyQueue(q[t]);
		clearVisitedTable(vtable[t]);
	}

	// Find the shortest depth
	long long int min = -1;
	for(int t=0; t<threads; t++){
		if(dtable[t] < 0)
			continue;
		if(min > dtable[t])
			min = dtable[t];
	}
	return min;
}

/*
 * Each different thread will use this function
 */
int shortest_path_parallel_worker(Graph *graph, VisitedTable *vtable, Queue *q,
		int depth, unsigned int v) {

	int threads = NUMBER_OF_THREADS;
	NEIGHBOR_NODE *neigh = NULL;
	unsigned int x = 0;

	//printf("Up to this point 1 %d\n");
	//fflush(stdout);

	while (!isEmpty(q)) {

		// Check depth table
		for(int i=0; i<threads; i++){
			if(dtable[i]<0)
				continue;

			// There is no reason to continue working
			//  a much shorter solution was found
			if(dtable[i]< depth)
				return depth;
		}


		dequeue(q, &neigh, &x, &depth);


		if (x == v)
			return depth;

		while (neigh != NULL) {
			for (int i = 0; i < neigh->size; i++) {
				unsigned int y = neigh->neighbors[i];
				if (!isVisited(vtable, y)) {
					setVisited(vtable, y);
					enqueue(q, getNeighbors(graph, y), y, depth + 1);
				}
			}
			neigh = neigh->next;
		}


	}

	return -1;
}

/*
 * De-allocates space allocated by graph
 */
void freeGraph(Graph** graph) {

	for (int i = 0; i < 1 << CLTABLEBITS; i++) {
		// Non empty cluster
		CLUSTER_NODE* cluster = (*graph)->clusters[i];
		if (!cluster)
			continue;

		// Free neighbors lists
		for (int j = 0; j < (1 << CLNODEBITS); j++) {

			if (!cluster->neighbors[j])
				continue;

			// Delete neighbor list
			freeNeighborList(cluster->neighbors[j]);
		}
		// Free cluster
		free((*graph)->clusters[i]);
	}

	// Free the entire graph
	free(*graph);

	// Now also free the dtable
	free(dtable);
}

// Hash table to index clusters
// 4MB * 8 byte pointers = 32MB
//CLUSTER_NODE *clusterTable[1 << CLTABLEBITS];

/*
 * Adds a new vertex in our graph structure
 *
 */
void addVertex(Graph* graph, unsigned int vertex) {

	int i = 0;

	// Find the right cluster to place the vertex in
	int index = vertex >> CLNODEBITS;

	// Cluster node uninitialized
	if (graph->clusters[index] == 0) {
		// Allocate space for cluster and add to our cluster table
		CLUSTER_NODE *cluster = (CLUSTER_NODE*) malloc(sizeof(CLUSTER_NODE));
		cluster->count = (unsigned int*)malloc(sizeof(unsigned int)* (1<<CLNODEBITS));
		graph->clusters[index] = cluster;

		// Set all nodes as if they don't have any neighbors
		for (i = 0; i < (1 << CLNODEBITS); i++){
			cluster->neighbors[i] = 0;
			cluster->count[i] = 0;
		}

	}
}

void deleteEdge(Graph* graph, unsigned int vertex1, unsigned int vertex2) {

	int index = vertex1 >> CLNODEBITS;
	int subindex = vertex1 & ((1 << CLNODEBITS) - 1);

	CLUSTER_NODE* cluster = graph->clusters[index];

	NEIGHBOR_NODE* neighbors = cluster->neighbors[subindex];

	/*
	 if(index == 1 && subindex == 2){
	 printf("Deleting edge %u -> %u\n",vertex1, vertex2);
	 fflush(stdout);
	 }
	 */

	if(removeNeighbor(vertex2, &neighbors));
		cluster->count[subindex]--;

	graph->clusters[index]->neighbors[subindex] = neighbors;

	/*

	 if(index == 1 && subindex == 2){
	 printNeighborList(neighbors);
	 fflush(stdout);
	 }
	 */

}

/*
 * Adds an edge between two vertices.
 * This function assumes that the edge connects two
 *  vertices that were already placed in the graph.
 */
void addEdge(Graph* graph, unsigned int vertex1, unsigned int vertex2) {

	int index = vertex1 >> CLNODEBITS;
	int subindex = vertex1 & ((1 << CLNODEBITS) - 1);

	CLUSTER_NODE* cluster = graph->clusters[index];
	NEIGHBOR_NODE* neighbors = cluster->neighbors[subindex];

	// If vertex did not have neighbors previously we need to allocate space for them
	if (!neighbors) {
		neighbors = createNode();
	}

	// Add neighbor vertex2 to vertex1
	addNeighbor(vertex2, neighbors);
	cluster->count[subindex]++;

	graph->clusters[index]->neighbors[subindex] = neighbors;

}

unsigned int getNeighborCount(Graph *g, unsigned int x){
	int index = x >> CLNODEBITS;
	int subindex = x & ((1 << CLNODEBITS) - 1);
	return g->clusters[index]->count[subindex];
}

NEIGHBOR_NODE* getNeighbors(Graph *g, unsigned int x) {
	int index = x >> CLNODEBITS;
	int subindex = x & ((1 << CLNODEBITS) - 1);
	return g->clusters[index]->neighbors[subindex];
}

#ifdef DEBUG_GRAPH

int main(int argc, char **argv) {

	printf("Test: graph.c\n");

	Graph *graph = (Graph *) malloc(sizeof(Graph));
	VisitedTable *vtable = (VisitedTable *) malloc(sizeof(VisitedTable));

	printf("Size of graph:%ld\n", size_of_graph(graph));

	initGraph(graph);
	initVisitTable(vtable);

// Parse file to create graph
	unsigned int vert1, vert2;
	char query;
//int code = 0;

// Read edges
	while (scanEdge(&vert1, &vert2)) {

		addVertex(graph, vert1);
		addVisitableNode(vtable, vert1);

		addVertex(graph, vert2);
		addVisitableNode(vtable, vert2);

		addEdge(graph, vert1, vert2);
	}
	printGraph(graph);

	unsigned int size = getVisitedTableSize(vtable);

	printf("VisitedTable size: %u bytes\n", size);
	printf("VisitedTable size: %u Kilo bytes\n", size / 1024);
	printf("VisitedTable size: %u Mega bytes\n", size / (1024 * 1024));
	printf("..Node overhead: %u bytes\n", size - 1024 * 1024 * 128);

	/*
	 // Visited table test
	 printf("..Test1: %d\n", isVisited(vtable, 3));
	 setVisited(vtable, 3);
	 printf("..Test2: %d\n", isVisited(vtable, 3));
	 printf("..Test3: %d\n", isVisited(vtable, 2));
	 setVisited(vtable, 2);
	 printf("..Test4: %d\n", isVisited(vtable, 2));

	 clearVisitedTable(vtable);
	 printf("..Test5: %d\n", isVisited(vtable, 3));
	 printf("..Test6: %d\n", isVisited(vtable, 2));

	 printf("S\n");
	 */

	//Test finding path
	printf("Path(%d,%d):%d\n",1,3,shortest_path(graph,vtable,1,3));
	printf("Path(%d,%d):%d\n",1,4,shortest_path(graph,vtable,1,4));
	printf("Path(%d,%d):%d\n",1,5,shortest_path(graph,vtable,1,5));
	printf("Path(%d,%d):%d\n",5,1,shortest_path(graph,vtable,5,1));
	free(graph);
	free(vtable);
	return 0;
}

#endif
