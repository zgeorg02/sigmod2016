#!/bin/bash

# Compile source code
gcc main.c -pg -g -c 
gcc graph.c -pg -g -c 
gcc neighbor_node.c -pg -g -c 
gcc parser.c -pg -g -c
gcc queue.c -pg -g -c 
gcc visited.c -pg -g -c

# Link object files
gcc -pg -g -fopenmp -o sigmod *.o
