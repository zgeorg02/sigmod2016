/*
 * Table that stores the neighbors of each vertex.
 */

#include"neighbor_node.h"

/*
 * Create a new neighbor node
 *
 */
NEIGHBOR_NODE* createNode() {

	NEIGHBOR_NODE* n = (NEIGHBOR_NODE*) (malloc(sizeof(NEIGHBOR_NODE)));

	if (n == NULL)
		return NULL;
	n->size = 0;
	n->neighbors = (unsigned int*) (malloc(sizeof(unsigned int) * ARRAY_SIZE));
	n->next = NULL;

	for (int i = 0; i < ARRAY_SIZE; i++)
		n->neighbors[i] = 0;

	//printf("CREATING NODE (val: %llu, address:%p)\n",n,&n);
	//fflush(stdout);

	return n;

}

/*
 * Adds a neighbor to the list
 *
 */
int addNeighbor(unsigned int n, NEIGHBOR_NODE* head) {

	NEIGHBOR_NODE* curr = head;

	//Traverse until find a node with null next or an empty index
	//in the neighbors array
	while (curr->size == ARRAY_SIZE && curr->next != NULL) {
		curr = curr->next;
	}

	if (curr->next == NULL && curr->size == ARRAY_SIZE) { //If the next is null and its full then create a new node
		NEIGHBOR_NODE* node = createNode();
		//printf("GOT NEW NODE (val: %llu, address:%p)\n",node,&node);
		//fflush(stdout);
		if (node == NULL){
			return 0;
		}
		node->neighbors[0] = n;
		(node->size)++;
		curr->next = node;
	} else { //else put the neighbor in the first empty space in the current neighbor array
		curr->neighbors[curr->size]=n;
		(curr->size)++;
	}

	return 1;

}

void printNeighborList(NEIGHBOR_NODE* node) {

	while (node != NULL) {
		for (int i = 0; i < ARRAY_SIZE; i++) {
			if (i < node->size)
				printf("%d ", node->neighbors[i]);
			else
				printf(".");
		}
		printf("-");
		node = node->next;
	}
	printf("\n");

}
/*
 * Removes a neighbor from the list
 * Returns status.
 * And also might need to change the head
 */
int removeNeighbor(unsigned int n, NEIGHBOR_NODE** head) {

	if (*head == NULL)
		return 0;

	NEIGHBOR_NODE* curr = *head;
	NEIGHBOR_NODE* prev = *head; //previous node for deletion
	int status = 0;

	//while (curr->next!=NULL && status!=1){ 
	do { //Check every node
		int i;
		for (i = 0; i < ARRAY_SIZE; i++) { //Check neighbor array of node


			if ((curr->neighbors)[i] == n) {

				// Delete node by pushing all right neighbors left
				for(int j=i+1; j<ARRAY_SIZE; j++){
					(curr->neighbors)[j-1] = (curr->neighbors)[j];
				}
				curr->size = curr->size -1;
				status = 1;

				//Check if its the last neighbor and delete node
				if (curr->size == 0) {

					// We need to make sure that we handle this correctly
					// If the node we are about to remove is the head
					if(curr == *head){

						//printf("DELETING A HEAD:\n");
						fflush(stdout);

						curr = (*head)->next;
						free((*head)->neighbors);
						free((*head));
						*head = curr;
					}
					else{
						prev->next = curr->next;
						free(curr->neighbors);
						free(curr);
					}
					// curr = NULL;
				}

				break;
			}
		}
		if (status == 1) //Break if we deleted a neighbor that leads to
			break; //freeing a node so that the following dont occur

		prev = curr;
		curr = curr->next;

	} while (curr != NULL);

	return status;

}

void freeNeighborList(NEIGHBOR_NODE* node) {

	while (node != NULL) {
		NEIGHBOR_NODE* temp = node->next;
		free(node->neighbors);
		free(node);
		node = temp;
	}

}
