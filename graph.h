/*
 * graph.h
 *
 *  Created on: Mar 1, 2016
 *      Author: zgeorg03
 */

#ifndef GRAPH_H
#define GRAPH_H

#include "neighbor_node.h"
#include "visited.h"
#include "queue.h"

// 256 nodes per cluster
typedef struct cluster_node {
	// Pointers to neighbors list for each node
	NEIGHBOR_NODE* neighbors[1 << CLNODEBITS];
	// An array that corresponds to the total number of neighbors in the list
	unsigned int* count;

} CLUSTER_NODE;

/**
 *
 */
typedef struct {

	CLUSTER_NODE * clusters[1 << CLTABLEBITS];

} Graph;

void initGraph(Graph* graph);
void printGraph(Graph* graph);
int shortest_path(Graph *graph, VisitedTable *visited, Queue *q, unsigned int u,
		unsigned int v);

int shortest_path_parallel(Graph *graph, VisitedTable **vtable, Queue **q,
		unsigned int u, unsigned int v, int threads);
int shortest_path_parallel_worker(Graph *graph, VisitedTable *vtable, Queue *q,
		int depth, unsigned int v, int threads);

void freeGraph(Graph** graph);
int scanEdge(unsigned int* vertex1, unsigned int* vertex2);
int scanQuery(char* query, unsigned int* vertex1, unsigned int* vertex2);
void addVertex(Graph* graph, unsigned int vertex);
void deleteEdge(Graph* graph, unsigned int vertex1, unsigned int vertex2);
void addEdge(Graph* graph, unsigned int vertex1, unsigned int vertex2);
unsigned int getNeighborCount(Graph *g, unsigned int x);
NEIGHBOR_NODE * getNeighbors(Graph *g, unsigned int x);
#endif /* GRAPH_H */
