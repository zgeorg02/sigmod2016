/*
 * visited.c
 *
 *  Created on: Mar 2, 2016
 *      Author: cothon01
 */

#include "visited.h"

/*
 * Initializes all 'cluster nodes' to zero, so that
 *  we know which nodes are unallocated
 */
void initVisitTable(VisitedTable *vtable) {
	for (int i = 0; i < (1 << CLTABLEBITS); i++)
		vtable->visitNode[i] = 0;
}

/*
 * Clears the bitArray of the visited node
 */
void clearVisitNode(VISIT_NODE* vnode) {
	for (int i = 0; i < (1 << CLNODEBITS) >> 3; i++)
		vnode->bitArray[i] = 0;
}

/*
 * Returns the total size of the entire structure in bytes.
 * It is inclusive of all pointers (also including the vtable pointer)
 * Sizes of the visited table are always equal or less than 256MBs
 */
unsigned int getVisitedTableSize(VisitedTable *vtable) {

	unsigned int total = sizeof(VisitedTable);

	for (int i = 0; i < (1 << CLTABLEBITS); i++) {

		VISIT_NODE* vnode = vtable->visitNode[i];
		if (!vnode)
			continue;

		total += sizeof(VISIT_NODE);
	}

	return total;
}

/*
 * Set a vertex as visit-able. This function is just pre-emptively
 *  allocate the cluster nodes needed
 */
void addVisitableNode(VisitedTable* vtable, unsigned int vertex) {

	int index = vertex >> CLNODEBITS;

	VISIT_NODE* vnode = vtable->visitNode[index];

	// If unallocated. Create new node
	if (vnode == 0) {
		vnode = (VISIT_NODE*) malloc(sizeof(VISIT_NODE));
		clearVisitNode(vnode);
		vtable->visitNode[index] = vnode;
	}
}

/*
 * Checks to see if a node / vertex was already visited.
 * Returns 0 for false, and anything else for true.
 */
#define USERFLAG1 ((1 << CLNODEBITS) - 1)
int isVisited(VisitedTable* vtable, unsigned int vertex) {

	int index = vertex >> CLNODEBITS;
	int subindex = vertex & ((1 << CLNODEBITS) - 1);

	VISIT_NODE* vnode = vtable->visitNode[index];

	int bitArrayIndex = subindex >> 3;
	byte bitArray = vnode->bitArray[bitArrayIndex];

	int bitArraySubIndex = subindex & 7;

	// Get right bit in our bitarray
	int bit = (bitArray >> bitArraySubIndex) & 1;

	return bit;
}

void setVisited(VisitedTable* vtable, unsigned int vertex) {

	int index = vertex >> CLNODEBITS;
	int subindex = vertex & ((1 << CLNODEBITS) - 1);

	VISIT_NODE* vnode = vtable->visitNode[index];

	int bitArrayIndex = subindex >> 3;
	byte bitArray = vnode->bitArray[bitArrayIndex];

	int bitArraySubIndex = subindex & 7;

	// Place a bit in the right place
	int bit = (1 << bitArraySubIndex);

	// Set bit using bitwise or
	bitArray |= bit;

	vnode->bitArray[bitArrayIndex] = bitArray;
	vtable->visitNode[index] = vnode;

}

/*
 * Clears table. All visited nodes will be forgotten.
 */
void clearVisitedTable(VisitedTable * vtable) {

	// Iterate for each node
	for (int i = 0; i < (1 << CLTABLEBITS); i++) {
		VISIT_NODE* vnode = vtable->visitNode[i];

		// If that node was initialized
		if (!vnode)
			continue;

		// Clear the bit-array contained within it
		clearVisitNode(vnode);
	}

}

void freeVisitTable(VisitedTable **vtable) {
	for (int i = 0; i < (1 << CLTABLEBITS); i++)
		if ((*vtable)->visitNode[i])
			free((*vtable)->visitNode[i]);
	free(*vtable);
}
