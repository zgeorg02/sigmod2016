/*
 * queue.h
 *
 *  Created on: Mar 6, 2016
 *      Author: zgeorg03
 */

#ifndef QUEUE_H_
#define QUEUE_H_

#include"defs.h"
#include "neighbor_node.h"
//24Bytes
typedef struct Node {
	// Tra
	NEIGHBOR_NODE * neighbors;
	unsigned int vertex;
	unsigned int depth;
	struct Node* next;
} Node;

//16Bytes
typedef struct Queue {
	Node *front;
	Node *rear;
	int size;
} Queue;

int initQueue(Queue **q);
int enqueue(Queue *q, NEIGHBOR_NODE *node, unsigned int vertex,
		unsigned int depth);
int dequeue(Queue *q, NEIGHBOR_NODE **x, unsigned int * vertex,
		unsigned int *depth);
void printQueue(Queue *q);
int isEmpty(Queue *q);
void clearQueue(Queue **q);
void emptyQueue(Queue *q);

#endif /* QUEUE_H_ */
