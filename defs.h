/*
 * defs.h
 *
 *  Created on: Mar 1, 2016
 *      Author: zgeorg03
 */

#ifndef DEFS_H_
#define DEFS_H_

#include<stdio.h>
#include<stdlib.h>
#include<omp.h>

#define CLNODEBITS 16 // 2^8 = 256 neighbour lists in a cluster node
#define CLTABLEBITS (32-CLNODEBITS)

#define NUMBER_OF_THREADS 8
#define MORE_THAN_A_SINGLE_THREAD_CAN_HANDLE 64
typedef unsigned char byte;

#define ARRAY_SIZE 256 //For neighbors in each node
#endif /* DEFS_H_ */
