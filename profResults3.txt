Flat profile:

Each sample counts as 0.01 seconds.
  %   cumulative   self              self     total           
 time   seconds   seconds    calls  ms/call  ms/call  name    
 59.34   1318.04  1318.04    59348    22.21    37.38  shortest_path
 16.29   1679.92   361.88 3232325890     0.00     0.00  getNeighbors
 12.60   1959.74   279.82 1782137700     0.00     0.00  isVisited
  6.43   2102.55   142.82 3232325890     0.00     0.00  setVisited
  2.33   2154.30    51.75 3232325890     0.00     0.00  enqueue
  0.85   2173.16    18.86  1483725     0.01     0.01  clearVisitNode
  0.74   2189.69    16.53 2588865186     0.00     0.00  isEmpty
  0.71   2205.46    15.77    59348     0.27     0.27  clearQueue
  0.34   2213.07     7.61    59348     0.13     0.45  clearVisitedTable
  0.23   2218.16     5.10    59348     0.09     0.09  initQueue
  0.13   2220.96     2.79  6506362     0.00     0.00  addVisitableNode
  0.02   2221.35     0.39   914758     0.00     0.00  createNode
  0.01   2221.55     0.21  3253181     0.00     0.00  addNeighbor
  0.00   2221.66     0.11  3253181     0.00     0.00  addEdge
  0.00   2221.75     0.09  3332858     0.00     0.00  getLine
  0.00   2221.84     0.09  3232856     0.00     0.00  scanEdge
  0.00   2221.88     0.04   100002     0.00     0.00  scanQuery
  0.00   2221.91     0.03        1    30.04    40.06  freeGraph
  0.00   2221.93     0.02    20326     0.00     0.00  removeNeighbor
  0.00   2221.94     0.01  6506362     0.00     0.00  addVertex
  0.00   2221.95     0.01   910761     0.00     0.00  freeNeighborList
  0.00   2221.96     0.01                             getVisitedTableSize
  0.00   2221.97     0.01                             main
  0.00   2221.98     0.01        1     5.01     5.01  initVisitTable
  0.00   2221.98     0.01                             printNeighborList
  0.00   2221.98     0.00    20326     0.00     0.00  deleteEdge
  0.00   2221.98     0.00        1     0.00     0.00  freeVisitTable
  0.00   2221.98     0.00        1     0.00     0.00  initGraph

 %         the percentage of the total running time of the
time       program used by this function.

cumulative a running sum of the number of seconds accounted
 seconds   for by this function and those listed above it.

 self      the number of seconds accounted for by this
seconds    function alone.  This is the major sort for this
           listing.

calls      the number of times this function was invoked, if
           this function is profiled, else blank.
 
 self      the average number of milliseconds spent in this
ms/call    function per call, if this function is profiled,
	   else blank.

 total     the average number of milliseconds spent in this
ms/call    function and its descendents per call, if this 
	   function is profiled, else blank.

name       the name of the function.  This is the minor sort
           for this listing. The index shows the location of
	   the function in the gprof listing. If the index is
	   in parenthesis it shows where it would appear in
	   the gprof listing if it were to be printed.

Copyright (C) 2012-2014 Free Software Foundation, Inc.

Copying and distribution of this file, with or without modification,
are permitted in any medium without royalty provided the copyright
notice and this notice are preserved.

		     Call graph (explanation follows)


granularity: each sample hit covers 2 byte(s) for 0.00% of 2221.98 seconds

index % time    self  children    called     name
                                                 <spontaneous>
[1]    100.0    0.01 2221.96                 main [1]
             1318.04  900.12   59348/59348       shortest_path [2]
                2.79    0.00 6506362/6506362     addVisitableNode [12]
                0.11    0.60 3253181/3253181     addEdge [13]
                0.09    0.09 3232856/3232856     scanEdge [16]
                0.04    0.00  100002/100002      scanQuery [18]
                0.03    0.01       1/1           freeGraph [19]
                0.00    0.02   20326/20326       deleteEdge [20]
                0.01    0.00 6506362/6506362     addVertex [22]
                0.01    0.00       1/1           initVisitTable [25]
                0.00    0.00       1/1           initGraph [28]
                0.00    0.00       1/1           freeVisitTable [27]
-----------------------------------------------
             1318.04  900.12   59348/59348       main [1]
[2]     99.8 1318.04  900.12   59348         shortest_path [2]
              361.88    0.00 3232325890/3232325890     getNeighbors [3]
              279.82    0.00 1782137700/1782137700     isVisited [4]
              142.82    0.00 3232325890/3232325890     setVisited [5]
               51.75    0.00 3232325890/3232325890     enqueue [6]
                7.61   18.86   59348/59348       clearVisitedTable [7]
               16.53    0.00 2588865186/2588865186     isEmpty [9]
               15.77    0.00   59348/59348       clearQueue [10]
                5.10    0.00   59348/59348       initQueue [11]
-----------------------------------------------
              361.88    0.00 3232325890/3232325890     shortest_path [2]
[3]     16.3  361.88    0.00 3232325890         getNeighbors [3]
-----------------------------------------------
              279.82    0.00 1782137700/1782137700     shortest_path [2]
[4]     12.6  279.82    0.00 1782137700         isVisited [4]
-----------------------------------------------
              142.82    0.00 3232325890/3232325890     shortest_path [2]
[5]      6.4  142.82    0.00 3232325890         setVisited [5]
-----------------------------------------------
               51.75    0.00 3232325890/3232325890     shortest_path [2]
[6]      2.3   51.75    0.00 3232325890         enqueue [6]
-----------------------------------------------
                7.61   18.86   59348/59348       shortest_path [2]
[7]      1.2    7.61   18.86   59348         clearVisitedTable [7]
               18.86    0.00 1483700/1483725     clearVisitNode [8]
-----------------------------------------------
                0.00    0.00      25/1483725     addVisitableNode [12]
               18.86    0.00 1483700/1483725     clearVisitedTable [7]
[8]      0.8   18.86    0.00 1483725         clearVisitNode [8]
-----------------------------------------------
               16.53    0.00 2588865186/2588865186     shortest_path [2]
[9]      0.7   16.53    0.00 2588865186         isEmpty [9]
-----------------------------------------------
               15.77    0.00   59348/59348       shortest_path [2]
[10]     0.7   15.77    0.00   59348         clearQueue [10]
-----------------------------------------------
                5.10    0.00   59348/59348       shortest_path [2]
[11]     0.2    5.10    0.00   59348         initQueue [11]
-----------------------------------------------
                2.79    0.00 6506362/6506362     main [1]
[12]     0.1    2.79    0.00 6506362         addVisitableNode [12]
                0.00    0.00      25/1483725     clearVisitNode [8]
-----------------------------------------------
                0.11    0.60 3253181/3253181     main [1]
[13]     0.0    0.11    0.60 3253181         addEdge [13]
                0.39    0.00  910761/914758      createNode [14]
                0.21    0.00 3253181/3253181     addNeighbor [15]
-----------------------------------------------
                0.00    0.00    3997/914758      addNeighbor [15]
                0.39    0.00  910761/914758      addEdge [13]
[14]     0.0    0.39    0.00  914758         createNode [14]
-----------------------------------------------
                0.21    0.00 3253181/3253181     addEdge [13]
[15]     0.0    0.21    0.00 3253181         addNeighbor [15]
                0.00    0.00    3997/914758      createNode [14]
-----------------------------------------------
                0.09    0.09 3232856/3232856     main [1]
[16]     0.0    0.09    0.09 3232856         scanEdge [16]
                0.09    0.00 3232856/3332858     getLine [17]
-----------------------------------------------
                0.00    0.00  100002/3332858     scanQuery [18]
                0.09    0.00 3232856/3332858     scanEdge [16]
[17]     0.0    0.09    0.00 3332858         getLine [17]
-----------------------------------------------
                0.04    0.00  100002/100002      main [1]
[18]     0.0    0.04    0.00  100002         scanQuery [18]
                0.00    0.00  100002/3332858     getLine [17]
-----------------------------------------------
                0.03    0.01       1/1           main [1]
[19]     0.0    0.03    0.01       1         freeGraph [19]
                0.01    0.00  910761/910761      freeNeighborList [23]
-----------------------------------------------
                0.00    0.02   20326/20326       main [1]
[20]     0.0    0.00    0.02   20326         deleteEdge [20]
                0.02    0.00   20326/20326       removeNeighbor [21]
-----------------------------------------------
                0.02    0.00   20326/20326       deleteEdge [20]
[21]     0.0    0.02    0.00   20326         removeNeighbor [21]
-----------------------------------------------
                0.01    0.00 6506362/6506362     main [1]
[22]     0.0    0.01    0.00 6506362         addVertex [22]
-----------------------------------------------
                0.01    0.00  910761/910761      freeGraph [19]
[23]     0.0    0.01    0.00  910761         freeNeighborList [23]
-----------------------------------------------
                                                 <spontaneous>
[24]     0.0    0.01    0.00                 getVisitedTableSize [24]
-----------------------------------------------
                0.01    0.00       1/1           main [1]
[25]     0.0    0.01    0.00       1         initVisitTable [25]
-----------------------------------------------
                                                 <spontaneous>
[26]     0.0    0.01    0.00                 printNeighborList [26]
-----------------------------------------------
                0.00    0.00       1/1           main [1]
[27]     0.0    0.00    0.00       1         freeVisitTable [27]
-----------------------------------------------
                0.00    0.00       1/1           main [1]
[28]     0.0    0.00    0.00       1         initGraph [28]
-----------------------------------------------

 This table describes the call tree of the program, and was sorted by
 the total amount of time spent in each function and its children.

 Each entry in this table consists of several lines.  The line with the
 index number at the left hand margin lists the current function.
 The lines above it list the functions that called this function,
 and the lines below it list the functions this one called.
 This line lists:
     index	A unique number given to each element of the table.
		Index numbers are sorted numerically.
		The index number is printed next to every function name so
		it is easier to look up where the function is in the table.

     % time	This is the percentage of the `total' time that was spent
		in this function and its children.  Note that due to
		different viewpoints, functions excluded by options, etc,
		these numbers will NOT add up to 100%.

     self	This is the total amount of time spent in this function.

     children	This is the total amount of time propagated into this
		function by its children.

     called	This is the number of times the function was called.
		If the function called itself recursively, the number
		only includes non-recursive calls, and is followed by
		a `+' and the number of recursive calls.

     name	The name of the current function.  The index number is
		printed after it.  If the function is a member of a
		cycle, the cycle number is printed between the
		function's name and the index number.


 For the function's parents, the fields have the following meanings:

     self	This is the amount of time that was propagated directly
		from the function into this parent.

     children	This is the amount of time that was propagated from
		the function's children into this parent.

     called	This is the number of times this parent called the
		function `/' the total number of times the function
		was called.  Recursive calls to the function are not
		included in the number after the `/'.

     name	This is the name of the parent.  The parent's index
		number is printed after it.  If the parent is a
		member of a cycle, the cycle number is printed between
		the name and the index number.

 If the parents of the function cannot be determined, the word
 `<spontaneous>' is printed in the `name' field, and all the other
 fields are blank.

 For the function's children, the fields have the following meanings:

     self	This is the amount of time that was propagated directly
		from the child into the function.

     children	This is the amount of time that was propagated from the
		child's children to the function.

     called	This is the number of times the function called
		this child `/' the total number of times the child
		was called.  Recursive calls by the child are not
		listed in the number after the `/'.

     name	This is the name of the child.  The child's index
		number is printed after it.  If the child is a
		member of a cycle, the cycle number is printed
		between the name and the index number.

 If there are any cycles (circles) in the call graph, there is an
 entry for the cycle-as-a-whole.  This entry shows who called the
 cycle (as parents) and the members of the cycle (as children.)
 The `+' recursive calls entry shows the number of function calls that
 were internal to the cycle, and the calls entry for each member shows,
 for that member, how many times it was called from other members of
 the cycle.

Copyright (C) 2012-2014 Free Software Foundation, Inc.

Copying and distribution of this file, with or without modification,
are permitted in any medium without royalty provided the copyright
notice and this notice are preserved.

Index by function name

  [13] addEdge                [19] freeGraph               [4] isVisited
  [15] addNeighbor            [23] freeNeighborList        [1] main
  [22] addVertex              [27] freeVisitTable         [26] printNeighborList
  [12] addVisitableNode       [17] getLine                [21] removeNeighbor
  [10] clearQueue              [3] getNeighbors           [16] scanEdge
   [8] clearVisitNode         [24] getVisitedTableSize    [18] scanQuery
   [7] clearVisitedTable      [28] initGraph               [5] setVisited
  [14] createNode             [11] initQueue               [2] shortest_path
  [20] deleteEdge             [25] initVisitTable
   [6] enqueue                 [9] isEmpty
