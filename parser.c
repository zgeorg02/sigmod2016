/*
 * A set of parsing utilities that will take the input from stdin as defined by
 * the competition. The goal is to do the parsing as fast as possible with the
 * least amount of redundancy.
 *
 */

/* $Id: scanf.c,v 1.1.1.1 2006/08/23 17:03:06 pefo Exp $ */

/*
 * Copyright (c) 2000-2002 Opsycon AB  (www.opsycon.se)
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *	This product includes software developed by Opsycon AB.
 * 4. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */
#include"defs.h"

// 10 characters for each 4byte integer
// 1 space between them
// 1 newline at the end
// 4 spaces just in case
// Also works for scanQuery with two extra spaces
#define MAXLN 32

// [0-9] numeric
// [SQAF] query commands
// [ ] delimeter
// [\n] line separation

int getLine(char *buffer, int maxSize) {

	int temp;
	int count = 0;
	while (count < maxSize) {

		// Get next character from standard input
		temp = getc((FILE*)stdin);
		if (temp == EOF)
			return -1;

		buffer[count++] = temp;

		// If newline add and stop
		if ((char) temp == '\n') {


			buffer[count] = 0;
			return (count);
		}
	}

	buffer[maxSize - 1] = 0;
	return count;
}

/*
 * Scans and finds next edge of our graph
 * vertex1: the first edge
 * vertex2: the second edge
 * Returns: 0 when 'S' is found
 * 			>0 when all is ok
 * 			<0 when somethings wrong
 */
int scanEdge(unsigned int* vertex1, unsigned int* vertex2) {

	char buffer[MAXLN];

	// Reads a whole line and puts to buffer
	if (getLine(buffer, MAXLN) <= 0)
		return (-1);

	//printf("%s\n", buffer);
	//printf("%d\n", buffer[0]);
	// If line starts with S
	if (buffer[0] == 'S')
		return 0;

	// Read first integer
	char* temp = (char*) buffer;

	unsigned int num = 0;
	while (*temp != ' ' && *temp != '\t') {
		num *= 10;
		num += (*temp) - '0';
		temp++;
	}
	*vertex1 = num;

	//printf("%u ");
	// Read second integer
	num = 0;
	temp++; // Skip space
	while (*temp != '\n') {
		num *= 10;
		num += (*temp) - '0';
		temp++;
	}
	*vertex2 = num;

	return 1;
}

/*
 * Scans and reads the next query
 * query: A letter signifying the query type (Q,A,D)
 * vertex1: the first edge
 * vertex2: the second edge
 * Returns: 0 when 'F' is found
 * 			>0 when all is ok
 * 			<0 when somethings wrong
 */
int scanQuery(char* query, unsigned int* vertex1, unsigned int* vertex2) {

	static char buffer[MAXLN];

	// Reads a whole line and put to buffer
	if (getLine(buffer, MAXLN) == 0)
		return (-1);

	// If line starts with F stop
	if (buffer[0] == 'F')
		return 0;

	// Read query symbol
	char* temp = (char*) buffer;
	*query = *temp;
	temp++;
	temp++;

	// Read first integer
	unsigned int num = 0;
	while (*temp != ' ' && *temp != '\t') {
		num *= 10;
		num += (*temp) - '0';
		temp++;
	}
	*vertex1 = num;

	// Read second integer
	num = 0;
	temp++; // Skip space
	while (*temp != '\n') {
		num *= 10;
		num += (*temp) - '0';
		temp++;
	}
	*vertex2 = num;

	return 1;
}
