#ifndef VISITED_H
#define VISITED_H

#include "defs.h"

// Each byte can hold data on 8 different nodes
typedef struct visit_node {
	byte bitArray[(1 << CLNODEBITS) >> 3];
} VISIT_NODE;

// The visitedtable will have the same form as our Graph
//  Basically it will comprise of clusters of data, so
//  that we use as little data as possible while offering
//  near O(1) performance.
typedef struct {
	VISIT_NODE * visitNode[1 << CLTABLEBITS];
} VisitedTable;

int isVisited(VisitedTable* vtable, unsigned int vertex);
void setVisited(VisitedTable* vtable, unsigned int vertex);
void clearVisitedTable(VisitedTable * vtable);
void initVisitTable(VisitedTable *vtable);
void clearVisitNode(VISIT_NODE* vnode);
unsigned int getVisitedTableSize(VisitedTable *vtable);
void addVisitableNode(VisitedTable* vtable, unsigned int vertex);
void freeVisitTable(VisitedTable **vtable);
#endif /* VISITED_H */
