/*
 * neighbor_node.h
 *
 *  Created on: Mar 1, 2016
 *      Author: zgeorg03
 */

#ifndef NEIGHBOR_NODE_H_
#define NEIGHBOR_NODE_H_

#include"defs.h"

typedef struct NEIGHBOR_NODE {
	unsigned int* neighbors;
	int size;
	struct NEIGHBOR_NODE* next;
} NEIGHBOR_NODE;
#endif /* NEIGHBOR_NODE_H_ */

NEIGHBOR_NODE* createNode();
int addNeighbor(unsigned int n, NEIGHBOR_NODE* head);
int removeNeighbor(unsigned int n, NEIGHBOR_NODE** head);
void printNeighborList(NEIGHBOR_NODE* node);
void freeNeighborList(NEIGHBOR_NODE* node);
