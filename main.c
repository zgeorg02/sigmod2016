/**
 * main.c
 *
 *  Created on: Feb 23, 2016
 *      Author: zgeorg03
 */

#include"main.h"



int main(int argc, char **argv) {

	// Set the number of threads
	omp_set_num_threads(NUMBER_OF_THREADS);
	int threads = NUMBER_OF_THREADS;

	// Initialize our graph
	Graph *graph = NULL;
	graph = (Graph *) malloc(sizeof(Graph));
	initGraph(graph);

	//VisitedTable *vtable = NULL;
	//vtable = (VisitedTable *) malloc(sizeof(VisitedTable));
	//initVisitTable(vtable);

	// Initialize our other two important structures
	VisitedTable **vtable = (VisitedTable**)malloc(sizeof(VisitedTable*)*threads);
	for(int i=0; i<threads; i++){
		vtable[i] = (VisitedTable*)malloc(sizeof(VisitedTable));
		initVisitTable(vtable[i]);
	}

	Queue **queue = (Queue**)malloc(sizeof(Queue*)*threads);
	for(int i=0; i<threads; i++){
		initQueue(&(queue[i]));
	}

	unsigned int vert1 = 0, vert2 = 0;
	char query = 0;
	//int code = 0;

	int column = 0;
	// Read edges
	while (1) {

		if (!scanEdge(&vert1, &vert2))
			break;

		//printf("col:%d, %u %u \n", column,vert1,vert2);

		addVertex(graph, vert1);
		// addVisitableNode(vtable, vert1);
		for(int t=0; t<threads; t++)
			addVisitableNode(vtable[t], vert1);

		addVertex(graph, vert2);
		// addVisitableNode(vtable, vert2);
		for(int t=0; t<threads; t++)
			addVisitableNode(vtable[t], vert2);

		addEdge(graph, vert1, vert2);

		column++;
	}
	printf("R\n");

	//printGraph(graph);
	//return 1;

	// Read queries
	while (1) {
		int code = scanQuery(&query, &vert1, &vert2);

		if (!code) {
			code = scanQuery(&query, &vert1, &vert2);

			if (code <= 0)
				break;
		}

		//printf("Query:%c, %u -> %u \n", query,vert1,vert2);
		//fflush(stdout);
		unsigned int ncount=0;
		switch (query) {

		case 'Q':

			ncount = getNeighborCount(graph,vert1);

			if(vert1 == vert2)
				printf("0\n");
			else{

				if(ncount == 0)
					printf("-1\n");
				else if(ncount == 1)
					printf("%d\n", shortest_path(graph, vtable[0],queue[0], vert1, vert2));
				else if (ncount < threads)
					printf("%d\n", shortest_path_parallel(graph, vtable,queue, vert1, vert2,ncount));
				else
					printf("%d\n", shortest_path_parallel(graph, vtable,queue, vert1, vert2,threads));
			}
			fflush(stdout);
			break;
		case 'A':

			addVertex(graph, vert1);
			// addVisitableNode(vtable, vert1);
			for(int t=0; t<threads; t++)
				addVisitableNode(vtable[t], vert1);

			addVertex(graph, vert2);
			// addVisitableNode(vtable, vert2);
			for(int t=0; t<threads; t++)
				addVisitableNode(vtable[t], vert2);

			addEdge(graph, vert1, vert2);
			break;
		case 'D':

			deleteEdge(graph, vert1, vert2);
			break;
		default:

			break;

		}

	}
	for(int t=0; t<threads; t++){
		freeVisitTable(&(vtable[t]));
		clearQueue(&(queue[t]));
	}
	free(vtable);
	free(queue);
	// freeVisitTable(&vtable);

	freeGraph(&graph);
	return 0;
}

