
# Sigmod 2016




### Quick Summary ###
We have attempted to create a program for the Sigmod 2016 contest but unfortunately we haven't been successful in passing the preliminary test. On the bright side, we have gained useful knowledge that might help others avoid our mistakes and hopefully succeed where we did not.

The theme of the contest was the construction and management of a large unweighted dynamic graph. After the initial construction of the data structure, the program must read queries of adding a new edge, deleting an edge or finding the minimum hop-distance from vertex A to vertex B. We were also notified that the graph could contain at most 2^32 vertices, and each vertex can be connected to 2^32.
The goal was to answer queries correctly (including insertions/deletions) as quickly as possible. Teams that wrote their program could submit it and after an evaluation it would be given a rank in a leader board.

### Our team ###
* Christos Othonos, 
* Zacharias Georgiou 
* Adamos Kyriakou. 