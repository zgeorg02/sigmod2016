/*
 * queue.c
 *
 *  Created on: Mar 6, 2016
 *      Author: zgeorg03
 */
#include"queue.h"

int initQueue(Queue **q) {
	*q = (Queue *) malloc(sizeof(Queue));
	(*q)->front = NULL;
	(*q)->rear = NULL;
	(*q)->size = 0;
	return 0;
}
// To Enqueue an integer
int enqueue(Queue *q, NEIGHBOR_NODE * node, unsigned int vertex,
		unsigned int depth) {
	Node* temp = (Node*) malloc(sizeof(struct Node));
	temp->neighbors = node;
	temp->depth = depth;
	temp->vertex = vertex;
	temp->next = NULL;
	(q->size)++;
	if (q->front == NULL && q->rear == NULL) {
		q->front = q->rear = temp;
		return 0;
	}
	q->rear->next = temp;
	q->rear = temp;

	return 0;
}

// To Dequeue an integer.
int dequeue(Queue *q, NEIGHBOR_NODE **x, unsigned int *vertex,
		unsigned int *depth) {
	Node* temp = q->front;

	if (q->front == NULL) {
		return -1;
	}
	if (q->front == q->rear) {
		q->front = q->rear = NULL;
	} else {
		q->front = q->front->next;
	}
	(q->size)--;
	*x = temp->neighbors;
	*depth = temp->depth;
	*vertex = temp->vertex;
	free(temp);
	temp = NULL;

	return 0;
}
void clearQueue(Queue **q) {
	Node *tmp;
	while ((*q)->front != NULL) {
		tmp = (*q)->front;

		(*q)->front = (*q)->front->next;
		free(tmp);
		tmp = NULL;
	}
	free(*q);
	*q = NULL;
}
void emptyQueue(Queue *q) {
	Node *tmp;
	while (q->front != NULL) {
		tmp = q->front;

		q->front = q->front->next;
		free(tmp);
		tmp = NULL;
	}
	q->front = NULL;
	q->rear = NULL;
	q->size = 0;
}

int isEmpty(Queue *q) {
	if (q->front == NULL)
		return 1;
	return 0;

}
void printQueue(Queue *q) {
	Node* temp = q->front;
	while (temp != NULL) {
		printNeighborList(temp->neighbors);
		temp = temp->next;
	}
	printf("\n");
}
